﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBController : MonoBehaviour {
    [Tooltip("Referencia al Sprite de caída del personaje")]
    public Sprite fallingSprite;

    [Tooltip("Referencia al Sprite de salto del personaje")]
    public Sprite jumpingSprite;

    [Tooltip("Referencia al Sprite de caída del personaje")]
    public Sprite deadSprite;

    [Space]

    [Tooltip("Referencia al GameManager")]
    public GameManager gameManager;

    [Tooltip("Referencia al SpriteRenderer del jugador")]
    public SpriteRenderer spriteRenderer;

    [Tooltip("Fuerza de gravedad que se quiere para el jugador")]
    public float gravity;

    [Tooltip("Altura de salto que se quiere para el jugador")]
    public float jumpHeight;

    [Tooltip("Velocidad de salto que se quiere para el jugador")]
    public float jumpSpeed;

    /// <summary>
    /// Variable privada para almacenar la altura que aún tiene que subir el personaje en el salto actual
    /// </summary>
    private float remainingJump;

    void Update() {
        // Comprueba si se ha terminado la partida
        if (!GameManager.gameOver) {
            // Comprueba si el jugador desea llevar a cabo la acción de salto
            bool jump = false;
#if UNITY_STANDALONE
            // Versión PC: tecla espacio
            jump = Input.GetKeyDown(KeyCode.Space);
#elif UNITY_ANDROID
            // Versión Android: toque en pantalla
            jump = ((Input.touchCount > 0) && (Input.GetTouch(0).phase == TouchPhase.Began));
#endif
            if (jump) {
                // Actualiza la altura que debe subir el personaje
                remainingJump = jumpHeight;
            }

            // Comprueba si el personaje está saltando y debe subir su posición
            if (remainingJump <= 0) {
                // Caída del personaje; mueve al jugador en el sentido negativo del eje Y
                transform.Translate(0, -gravity, 0);
                // Se modifica el sprite del personaje
                spriteRenderer.sprite = fallingSprite;
            } else {
                // Salto del personaje; mueve al jugador en el sentido positivo del eje Y según la velocidad indicada
                transform.Translate(0, jumpSpeed, 0);
                // Se modifica el sprite del personaje
                spriteRenderer.sprite = jumpingSprite;
                // Actualiza la altura que aún queda por subir
                remainingJump -= jumpSpeed;
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        // El personaje ha colisionado con algún objeto, luego se debe terminar la partida
        gameManager.PlayerDied();
        // Se modifica el sprite del personaje
        spriteRenderer.sprite = deadSprite;
    }
}