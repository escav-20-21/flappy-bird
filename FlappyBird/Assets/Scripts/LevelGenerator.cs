﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour {
    [Tooltip("Referencia al prefab de columna")]
    public GameObject columnPrefab;

    [Space]

    [Tooltip("Ratio con el que deben generarse las columnas (en segundos)")]
    public float columnRatio;

    [Space]

    [Tooltip("Posición X mínima en que debe instanciarse cada columna")]
    public float minColumnXPos;

    [Tooltip("Posición X máxima en que debe instanciarse cada columna")]
    public float maxColumnXPos;

    [Tooltip("Posición Y mínima en que debe instanciarse cada columna")]
    public float minColumnYPos;

    [Tooltip("Posición Y máxima en que debe instanciarse cada columna")]
    public float maxColumnYPos;

    private void Start() {
        // Hace que la función para instanciar obstáculos se ejecute con la frecuencia especificada
        InvokeRepeating("NewColumn", 1, columnRatio);
    }

    private void NewColumn() {
        // Comprueba si se ha terminado la partida
        if (!GameManager.gameOver) {
            // Calcula la posición en que debe instanciarse la columna aleatoriamente
            Vector3 position = new Vector3(Random.Range(minColumnXPos, maxColumnXPos), Random.Range(minColumnYPos, maxColumnYPos), 0);

            // Instancia la columna en la posición previamente calculada y con la rotación base (0, 0, 0)
            Instantiate(columnPrefab, position, Quaternion.identity);
        }
    }
}