﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMovement : MonoBehaviour {
    [Tooltip("Referencia la material del fondo")]
    public Material backgroundMaterial;

    [Tooltip("Velocidad inicial de movimiento del fondo")]
    public float initialMovementSpeed;

    // Velocidad real de movimiento
    private float movementSpeed;

    void Update() {
        // Comprueba si se ha terminado la partida
        if (!GameManager.gameOver) {
            // ADICIONAL: La velocidad del obstáculo aumenta progresivamente (0.01 cada 10 segundos)
            movementSpeed = initialMovementSpeed + 0.01f * (Time.timeSinceLevelLoad / 10);

            // Modifica el offset de la textura para simular el movimiento horizontal del jugador
            backgroundMaterial.SetTextureOffset("_MainTex", new Vector2(Time.time * movementSpeed, 0));
        }
    }
}