﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointsTrigger : MonoBehaviour {

    private void OnEnable() {
        // ADICIONAL: Al reactivar el obstáculo en el Object Pooling, se reactiva el Collider2D también
        GetComponent<Collider2D>().enabled = true;
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        // Comprueba si es el jugador el que ha entrado
        if (collision.gameObject.CompareTag("Player")) {
            // Añade 1 punto y desactiva el trigger para no contar puntos varias veces
            ++GameManager.score;
            GetComponent<Collider2D>().enabled = false;
        }
    }
}