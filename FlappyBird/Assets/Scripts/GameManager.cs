﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [Tooltip("Referencia al componente Text de la interfaz que contiene la puntuación")]
    public Text scoreText;

    [Tooltip("Referencia al componente Text de la interfaz que contiene el mensaje al lograr un nuevo récord de puntuación")]
    public Text newRecordMessageText;

    /// <summary>
    /// Almacena si la partida ha finalizado
    /// </summary>
    public static bool gameOver;

    /// <summary>
    /// Almacena la puntuación del jugador
    /// </summary>
    public static int score;

    private void Start() {
        // Se asegura que las variables estáticas se inicialicen correctamente
        gameOver = false;
        score = 0;
    }

    private void Update() {
        // Actualiza la puntuación del jugador
        scoreText.text = score.ToString();

    }

    public void PlayerDied() {
        // Registra que el jugador ha muerto
        gameOver = true;
        // Comprueba si se ha obtenido un nuevo récord de puntuación
        if (score > PlayerPrefs.GetInt("BestScore", 0)) {
            // Registra el nuevo récord
            PlayerPrefs.SetInt("BestScore", score);
            // Muestra el correspondiente mensaje en la interfaz
            newRecordMessageText.enabled = true;
        }
    }
}