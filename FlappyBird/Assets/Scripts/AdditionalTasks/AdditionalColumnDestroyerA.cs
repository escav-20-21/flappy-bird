﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdditionalColumnDestroyerA : MonoBehaviour {

    private void OnCollisionEnter2D(Collision2D collision) {
        // Desactiva el objeto con el que colisiona si se trata de una columna (comprueba el padre, los hijos son los que tienen los colliders)
        if (collision.transform.parent.CompareTag("Column")) {
            collision.transform.parent.gameObject.SetActive(false);
            // Según el tipo de obstáculo, lo añade al listado correspondiente para el ObjectPooling
            if (collision.gameObject.name.Contains("Basic")) {
                AdditionalLevelGenerator.disabledBasicColumns.Add(collision.gameObject);
            } else if (collision.gameObject.name.Contains("Additional01")) {
                AdditionalLevelGenerator.disabledAdd01Columns.Add(collision.gameObject);
            } else if (collision.gameObject.name.Contains("Additional02")) {
                AdditionalLevelGenerator.disabledAdd02Columns.Add(collision.gameObject);
            }
        }
    }
}