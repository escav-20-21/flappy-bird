﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdditionalColumnDestroyerB : MonoBehaviour {
    [Tooltip("Posición del eje X en que la columna debe destruirse")]
    public float xPos;

    private void Update() {
        // Comprueba si la columna ha llegado a la posición en la que debe desactivarse
        if (transform.position.x <= xPos) {
            gameObject.SetActive(false);
            // Según el tipo de obstáculo, lo añade al listado correspondiente para el ObjectPooling
            if (gameObject.name.Contains("Basic")) {
                AdditionalLevelGenerator.disabledBasicColumns.Add(gameObject);
            } else if (gameObject.name.Contains("Additional01")) {
                AdditionalLevelGenerator.disabledAdd01Columns.Add(gameObject);
            } else if (gameObject.name.Contains("Additional02")) {
                AdditionalLevelGenerator.disabledAdd02Columns.Add(gameObject);
            }
        }
    }
}