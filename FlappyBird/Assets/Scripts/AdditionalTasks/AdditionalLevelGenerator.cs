﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdditionalLevelGenerator : MonoBehaviour {
    [Tooltip("Referencia al prefab básico de columna")]
    public GameObject basicColumnPrefab;

    [Tooltip("Referencia a un prefab adicional de columna")]
    public GameObject additionalColumnPrefab01;

    [Tooltip("Referencia a otro prefab adicional de columna")]
    public GameObject additionalColumnPrefab02;

    [Space]

    [Tooltip("Ratio con el que deben generarse las columnas (en segundos)")]
    public float columnRatio;

    [Space]

    [Tooltip("Posición X mínima en que debe instanciarse cada columna")]
    public float minColumnXPos;

    [Tooltip("Posición X máxima en que debe instanciarse cada columna")]
    public float maxColumnXPos;

    [Tooltip("Posición Y mínima en que debe instanciarse cada columna")]
    public float minColumnYPos;

    [Tooltip("Posición Y máxima en que debe instanciarse cada columna")]
    public float maxColumnYPos;

    // Listas con las columnas para Object Pooling
    public static List<GameObject> disabledBasicColumns;
    public static List<GameObject> disabledAdd01Columns;
    public static List<GameObject> disabledAdd02Columns;

    private void Start() {
        // Inicialización de las listas con los objetos desactivados para llevar a cabo el Object Pooling
        disabledBasicColumns = new List<GameObject>();
        disabledAdd01Columns = new List<GameObject>();
        disabledAdd02Columns = new List<GameObject>();
        // Hace que la función para instanciar obstáculos se ejecute con la frecuencia especificada
        InvokeRepeating("NewColumn", 1, columnRatio);
    }

    private void NewColumn() {
        // Comprueba si se ha terminado la partida
        if (!GameManager.gameOver) {
            // Calcula la posición en que debe instanciarse la columna aleatoriamente
            Vector3 position = new Vector3(Random.Range(minColumnXPos, maxColumnXPos), Random.Range(minColumnYPos, maxColumnYPos), 0);

            // Decide aleatoriamente qué tipo de obstáculo se va a instanciar
            int rnd = Random.Range(0, 10);
            GameObject column;
            if (rnd == 0) { // 10% probabilidad
                // Comprueba si hay columna desactivada sin utilizar o se debe instanciar
                if (disabledAdd01Columns.Count == 0) {
                    disabledAdd01Columns.Add(Instantiate(additionalColumnPrefab01, transform.position, Quaternion.identity));
                }
                // Selecciona la primera columna del listado y la borra del listado
                column = disabledAdd01Columns[0];
                disabledAdd01Columns.RemoveAt(0);
            } else if (rnd < 3) { // 20% probabilidad
                // Comprueba si hay columna desactivada sin utilizar o se debe instanciar
                if (disabledAdd02Columns.Count == 0) {
                    disabledAdd02Columns.Add(Instantiate(additionalColumnPrefab02, transform.position, Quaternion.identity));
                }
                // Selecciona la primera columna del listado y la borra del listado
                column = disabledAdd02Columns[0];
                disabledAdd02Columns.RemoveAt(0);
            } else { // 70% probabilidad
                // Comprueba si hay columna desactivada sin utilizar o se debe instanciar
                if (disabledBasicColumns.Count == 0) {
                    disabledBasicColumns.Add(Instantiate(basicColumnPrefab, transform.position, Quaternion.identity));
                }
                // Selecciona la primera columna del listado y la borra del listado
                column = disabledBasicColumns[0];
                disabledBasicColumns.RemoveAt(0);
            }

            // Coloca la columna en la posición previamente calculada, con la rotación base (0, 0, 0) y se asegura que queda activada
            column.transform.SetPositionAndRotation(position, Quaternion.identity);
            column.SetActive(true);
        }
    }
}
