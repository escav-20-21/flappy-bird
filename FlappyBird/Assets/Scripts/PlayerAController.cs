﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAController : MonoBehaviour {
    [Tooltip("Referencia al Sprite de caída del personaje")]
    public Sprite fallingSprite;

    [Tooltip("Referencia al Sprite de salto del personaje")]
    public Sprite jumpingSprite;

    [Tooltip("Referencia al Sprite de caída del personaje")]
    public Sprite deadSprite;

    [Space]

    [Tooltip("Referencia al GameManager")]
    public GameManager gameManager;

    [Tooltip("Referencia al Rigidbody del jugador")]
    public Rigidbody2D rb;

    [Tooltip("Referencia al SpriteRenderer del jugador")]
    public SpriteRenderer spriteRenderer;

    [Tooltip("Referencia al sistema de partículas a ejecutar durante el salto")]
    public ParticleSystem jumpPS;

    [Tooltip("Fuerza que se quiere para el salto del jugador")]
    public float jumpForce;

    void Update() {
        // Comprueba si se ha terminado la partida
        if (!GameManager.gameOver) {
            // Comprueba si el jugador pulsa la tecla de salto (espacio)
            if (Input.GetKeyDown(KeyCode.Space)) {
                // Aplica la fuerza del salto
                rb.velocity = jumpForce * Vector2.up;
                jumpPS.Play();
            }
            // Se actualiza el sprite según el movimiento del personaje
            if (rb.velocity.y > 0) {
                // El personaje se mueve en el sentido positivo del eje Y, hacia arriba
                spriteRenderer.sprite = jumpingSprite;
            } else {
                // El personaje se mueve en el sentido negativo del eje Y, hacia abajo
                spriteRenderer.sprite = fallingSprite;
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        // El personaje ha colisionado con algún objeto, luego se debe terminar la partida
        gameManager.PlayerDied();
        // Se modifica el sprite del personaje
        spriteRenderer.sprite = deadSprite;
    }
}
