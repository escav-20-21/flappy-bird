﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleController : MonoBehaviour {
    [Tooltip("Velocidad inicial de movimiento del obstáculo")]
    public float initialMovementSpeed;

    [Tooltip("ADICIONAL: ¿Posee el obstáculo 2 partes?")]
    public bool twoParts;

    // Velocidad real de movimiento
    private float movementSpeed;

    private void Start() {
        // ADICIONAL: Modifica el espacio existente entre las dos partes del obstáculo (si las tiene)
        if (twoParts) {
            // Parte de arriba del obstáculo
            transform.Find("Top").Translate(0, Random.Range(-0.5f, 0), 0, Space.World);
            // Parte de abajo del obstáculo
            transform.Find("Bottom").Translate(0, Random.Range(0, 0.5f), 0, Space.World);
        }
    }

    private void Update() {
        // Comprueba si se ha terminado la partida
        if (!GameManager.gameOver) {
            // ADICIONAL: La velocidad del obstáculo aumenta progresivamente (0.01 cada 10 segundos)
            movementSpeed = initialMovementSpeed + 0.01f * (Time.timeSinceLevelLoad / 10);

            // Movimiento del obstáculo hacia la izquierda (sentido negativo eje X)
            transform.Translate(-movementSpeed, 0, 0);
        }
    }

}