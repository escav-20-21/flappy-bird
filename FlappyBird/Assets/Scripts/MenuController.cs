﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {
    [Tooltip("Referencia al panel de Game Over")]
    public GameObject gameOverPanel;

    private void Update() {
        // Comprueba si se debe mostrar el panel de Game Over
        if (GameManager.gameOver) {
            gameOverPanel.SetActive(true);
        }
    }

    public void Menu() {
        // Carga la escena del menú
        SceneManager.LoadScene("Menu");
    }

    public void PlayLevel() {
        // Carga la escena de juego
        SceneManager.LoadScene("Game");
    }

    public void QuitGame() {
        // Sale del juego
        Application.Quit();
    }
}