﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColumnDestroyerB : MonoBehaviour {
    [Tooltip("Posición del eje X en que la columna debe destruirse")]
    public float xPos;

    private void Update() {
        // Comprueba si la columna ha llegado a la posición en la que debe destruirse
        if (transform.position.x <= xPos) {
            Destroy(gameObject);
        }
    }
}
