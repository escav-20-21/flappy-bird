﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColumnDestroyerA : MonoBehaviour {

    private void OnCollisionEnter2D(Collision2D collision) {
        // Destruye el objeto con el que colisiona si se trata de una columna (comprueba el padre, los hijos son los que tienen los colliders)
        if (collision.transform.parent.CompareTag("Column")) {
            Destroy(collision.transform.parent.gameObject);
        }
    }
}