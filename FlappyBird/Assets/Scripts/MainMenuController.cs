﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour {
    [Tooltip("Referencia al componente Text de la interfaz que contiene la máxima puntuación")]
    public Text bestScoreText;

    private void Start() {
        // Actualiza la interfaz con la mejor puntuación recibida
        bestScoreText.text += PlayerPrefs.GetInt("BestScore", 0);
    }

    public void PlayLevel() {
        // Carga la escena de juego
        SceneManager.LoadScene("Game");
    }

    public void QuitGame() {
        // Sale del juego
        Application.Quit();
    }
}